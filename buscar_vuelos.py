from __future__ import print_function
import httplib2
import os
from interjet_phantom import Interjet
from sendEmail import Correo
import datetime

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main():
    """Shows basic usage of the Sheets API.

    Creates a Sheets API service object and prints the names and majors of
    students in a sample spreadsheet:
    https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    spreadsheetId = '1gcONVrVxLqDWiVRYVQ_ZFhouP2PMGUaiYNV9EMqzQY8'
    rangeName = 'Hoja 1!A2:U'
    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheetId, range=rangeName).execute()
    values = result.get('values', [])

    if not values:
        print('No data found!')
    else:
        sender = Correo()
        for row in values:# Compropbar si está suscrito a esta hora
            hora_actual = datetime.datetime.now().hour
            if (len(row) >= hora_actual):
                if (row[hora_actual -1] == 'si'):
                    try :                
                        # Obtener datos
                        name = row[0]
                        ini_date = datetime.datetime.strptime(row[1],'%d/%m/%Y').date()
                        end_date = datetime.datetime.strptime(row[2],'%d/%m/%Y').date()
                        email_add = row[3]
                        origin = row[4]
                        destination = row[5]
                        email_str = "%s, acá tienes el reporte de los precios para vuelos de %s a %s entre el %s y el %s:\n"\
                                    % (name,origin,destination,ini_date,end_date)
        
                        # Buscar vuelos
                        airline = Interjet(ini_date, end_date, origin, destination)
                        result = airline.search()                
                        airline.terminate()
        
                        # Enviar Correo
                        sender.sendmail(email_add,email_str + result)
                    except Exception as e:   
                        try:             
                            airline.terminate()
                        except:
                            pass
                        print(e)


if __name__ == '__main__':
    main()
