# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Este software permite buscar vuelos en un intervao de fechas dado por un usuario en un documento compartido de Google.
 El repote con los precios se envía por correo.
* Version 0.00000000

### How do I get set up? ###

* Instalar selenium (https://techarena51.com/blog/install-selenium-linux-automate-web-tests/)
* Probablemente tengas problemas con geckodriver (http://stackoverflow.com/questions/40208051/selenium-using-python-geckodriver-executable-needs-to-be-in-path)
* Dependencias:
  - Python 3
* Correr pruebas
  * python sendEmail.py
  * python interjet.py
  * python buscar_vuelos.py
* Poner en cron
 * */30  *   * * *    root    su vladimir -c "DISPLAY=:0.0 /home/vladimir/Documentos/SOFTWARE/python/selenium/buscar_vuelos.sh"

### Contribution guidelines ###

* Documentar y reparar lo q veas
* Code review
* Hacer disponible busquedas en Aeroméxico

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
