from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time, datetime

MONTHS = {1:'ene',2:'feb',3:'mar',4:'abr',5:'may',6:'jun',7:'jul',8:'ago',9:'sep',10:'oct',11:'nov',12:'dic'}
url = "https://www.interjet.com.mx"

class Interjet:
    # Constructor
    def __init__(self, ini_date, end_date, origin, destino):
        self.ini_date = ini_date
        self.mon_ini = ini_date.month
        self.year = ini_date.year
        self.end_date = end_date
        self.mon_end = end_date.month
        self.origin = origin
        self.destino = destino
        self.driver = webdriver.Firefox()

    # Devuelve el numero del mes dado  como str
    def numeroDelMes(self,month):
        for key in MONTHS:
            if (MONTHS[key] == month):
                break
        return key

    # Moverse x el calendario
    def irAlMes(self,month):
            # Localizar el mes
            elements = self.driver.find_elements_by_tag_name("strong")
            for elem in elements:
                if (elem.text == MONTHS[month]):
                    elem.click() # Select desired month
            # Esperar a que el mes esté listo
            time.sleep(10)

    # Do de job
    def search(self):
        self.driver.get(url)
        assert "Interjet" in self.driver.title
        
        # Poner en español
        elem = self.driver.find_element_by_class_name("es-mx")
        elem.click()
        elem.send_keys(Keys.DOWN)
        elem.send_keys(Keys.RETURN)  
          
        wait = WebDriverWait(self.driver, 20)
        element = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@value='Buscar vuelos']")))
        # Esperar a que el mes esté listo
        time.sleep(10)

        # Solo ida
        elem = self.driver.find_element_by_id("ControlGroupHomeView_AvailabilitySearchInputHomeView_OneWay")
        elem.click()

        # Calendario
        elem = self.driver.find_element_by_id("SkysalesSearchFlightByPrice")
        elem.click()

        # Editar el origen
        elem = self.driver.find_element_by_id("OriginStation")
        elem.clear()
        elem.send_keys(self.origin)
        time.sleep(3)
        elem.send_keys(Keys.DOWN)
        elem.send_keys(Keys.TAB)

        # Editar el destino
        elem = self.driver.find_element_by_id("DestinationStation")
        elem.clear()
        elem.send_keys(self.destino)
        time.sleep(3)
        elem.send_keys(Keys.DOWN)
        elem.send_keys(Keys.TAB)

        elem = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "ControlGroupHomeView_AvailabilitySearchInputHomeView_ButtonSubmit"))
            )
        #elem = driver.find_element_by_class_name("ControlGroupHomeView_AvailabilitySearchInputHomeView_ButtonSubmit")
        elem.click()

        # Esperar a que carge
        element = wait.until(EC.element_to_be_clickable((By.ID, 'ControlGroupCalendarSelectView_ButtonSubmit')))

        result = ""
        menor_precio = 50000 #Ridiculamente grande
        mejor_fecha = ""

        mon = self.mon_ini
        while(mon <= self.mon_end):
            self.irAlMes(mon)
            elements = self.driver.find_elements_by_class_name("day")
            for elem in elements:
                a = elem.text.split("\n")
                fecha = datetime.date(self.year,self.numeroDelMes(a[0].split("/")[0]),int(a[0].split("/")[1]))
                if (fecha >= self.ini_date):
                    if (fecha <= self.end_date):
                        # Intervalo deseado
                        if (len(a)>1):
                            # No está vacío
                            precio = int(a[1].split("$")[1].replace(",", ""))
                            result += str(fecha) + ": $"+str(precio) +  "\n"
                            if (precio < menor_precio):
                                menor_precio = precio
                                mejor_fecha = str(fecha)
                    else:
                        break # Ya sobrepasamos la ultima fecha deseada
            mon += 1

        return result + "\nMejor precio: $%i el %s" % (menor_precio,mejor_fecha)


# Terminar
    def terminate(self):
        self.driver.quit()

if __name__ == "__main__":
    day_ini = 12
    day_end = 20
    mon_ini = 7
    mon_end = 8
    year = 2017
    ini_date = datetime.date(year,mon_ini,day_ini)
    end_date = datetime.date(year,mon_end,day_end)
    origin = "La Habana"
    destino = "Ciudad de México"

    # Test the class
    airline = Interjet(ini_date, end_date, origin, destino)
    print(airline.search())
    airline.terminate()
